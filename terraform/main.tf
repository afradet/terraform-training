terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"

  default_tags {
    tags = {
      afr-project = "tftraining"
      afr-manager = "tf"
    }
  }
}


resource "aws_s3_bucket" "FrontDeploymentBucket" {
  bucket = "tftraining.afradet.fr"
  force_destroy = true
  object_lock_enabled = false
}